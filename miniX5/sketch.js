//generative art displaying different shapes in different color schemes

let size1 = 5
let size2 = 100

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(25);
}

//random display of either ellipses or rectangles
function draw() {
  if (random(10)<5){
    stroke(0, random(255), random(255), random(255));
    strokeWeight(random(0,10))
    noFill();
      rect(random(windowWidth), random(windowHeight), random(size1, size2), random(size1, size2));
  } else {
    //if not pressed it will display ellipses in a red and blue color scheme
      stroke(random(255), 0, random(255), random(255));
      strokeWeight(random(1,10))
      noFill();
        ellipse(random(windowWidth), random(windowHeight), random(size1, size2), random(size1, size2));
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
