**NB:** I missed the part in the brief that at least one for loop so now its unfortunately not there

*View my project [here](https://unblissful.gitlab.io/aesthetic-programming/miniX5/)*

*View my code [here](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/blob/main/miniX5/sketch.js)*

![](preview.png)

#### What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behaviour?


The rules in my generative program:
* The program should create a new pattern every time it's refreshed
* the same figure should be different in shape (i.e., length, width etc.) and size
* The program should display 2 different geometric shapes
* Shapes should form throughout the whole canvas

Over time my program will create more and more shapes, the longer you wait and thus creating a stacked and overlapping image going from a black screen to one filled with different colours, and if stacked enough making it look like an often seen background for a 'traditional' canvas painting or perhaps splatter or graffiti art.

I've used and `if-else` statement to have program changed between the red/blue ellipses and green/blue rectangles at random.

In this case the program created with the simple rules generates a pattern that continuously builds upon it self and consequently creating a more complex result from four simple rules.

___
#### What role do rules and processes have in your work?

The rules in my program are not very demanding and therefore works more as a slight guideline for the program. The rules do not demand certain colours, specific sizes and shapes or placement thereof. The rules are very broad and leaves you with many possibilities depending on how one would interpret it.

The different shapes, sizes and in this case colours further affects the outcome of the program resulting in a more dynamic looking piece.

And as previously mentioned the figures sort of stack and build and thus the longer you let the program run the more figures will appear and the more saturated and less black it will become.
___
#### Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?*

In the assigned readings there's been mentions of the pseudo-randomness of such computed generative art, the program won't really be completely random as there are rules set and you code to 'auto generate' something and therefore someone would've made some deciding factors for the work and as a result it would never be completely random.

As mentioned in 'Aesthetic Programming' by Soon & Cox(2020) rules aren't utilised in their work by just programmers but also in crafts such as knitting and crocheting. In knitting or crocheting following such rules or a pattern often creates a specific result because you're following a pattern to knit or crochet something particular. But if you did not disregard these rules, but instead followed them loosely and made them operate in the same manner as the simple rules in this miniX, then you'd be able to create multiple different products of, say, different colours and sizes. So on that account the rules still hinder som randomness but depending on how much creative freedom these rules allow, there's place for more or less randomness and more or less creative control. 

Because the rules I've worked with aren't the most set rules and opens up for a lot of interpretation, it offers a higher level of control in the creative process opposed to if the rules would appear to leave you with less options for the visual outcome.


---
##### References:
* Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
* Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
