*View my project [here](https://unblissful.gitlab.io/aesthetic-programming/miniX4/)*

*View my code [here](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/blob/main/miniX4/sketch.js)*

![](preview1.png)
![](preview2.png)


In the contemporary world with a society closely tied to all sorts of software and algorithms, the urge and potential to share your life down to the tiniest detail is made possible and accessible through various media.
A modern society that allows and creates space for an open and more so amenable conversation about multiple topics such as gender.

The willingness to share and perhaps even feeling like you *need* to share, results in sharing a lot more data than initially intended.

With this miniX I've had Facebook in mind and the many choices of gender they provide. I had previously not taken much consideration to my gender or gender in general and had therefore not been away of the possibility to choose amongst such a wide variety of genders on Facebook. Although knowing of a lot of different genders prior, I had only come to know about the availability on Facebook when I visited the gender museum - KØN and was once again reminded of it when reading "Data capture" in *Aesthetic Programming: A Handbook of Software Studies* (Soon & Cox)

> *Researcher Rena Bivens has made a thorough analysis of Facebook's registration page in relation to the gender options available. When Facebook was first launched in 2004 there was no gender field, but things changed in 2008 when a dropdown list was introduced that consisted solely of the options Male or Female, further changed with the use of radio buttons to emphasize the binary choice. A breakthrough occured in 2014 when Facebook allowed users to customize the gender field and you can now select from a  list of more than 50 gender options.*

With this in mind I created a miniX that shows not quite 50 genders but more than the binary which appear when the button saying 'store your gender here' is pressed. The idea is that your gender is likely 'stored' here when you press the button, as in your gender will likely appear on screen (unless of course it's something I haven't added) and so your gender will appear to be stored here and the more times you click the button the more times the genders will appear and thus creating a resemblance to more and more data being stored sort of a visual portrayal of how the more people who chooses a gender on Facebook the more data they will have stored overall but also in a sense of, the more info you input about yourself the more data you share, not only on Facebook but wherever it might be.

I've done this by using `createButton()` and then using `button.mousePressed()` wherein I called a function I made where i utilise `for() loops` to display an array `[]` where I wrote the genders.

Besides the use of a button to create a sense of stored data I chose to add `createCapture` to add video from your webcam. By placing the button right underneath the video from the webcam a relation between the button and the video transpires, suggesting the video of oneself as a means of seeing your gender or as a mirror and reflection upon your gender and it being something you can 'store' so to speak.
___

##### References:
* Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
* “p5 DOM reference,” https://p5js.org/reference/#group-DOM.
* "p5 input and button example," https://p5js.org/examples/dom-input-and-button.html
