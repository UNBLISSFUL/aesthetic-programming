
//array with gender expression/identity
let gender = ['afab', 'agender', 'aliagender', 'amab', 'androgyne', 'aporagender', 'bigender', 'butch', 'cisgender', 'demiboy', 'demienby', 'demigender', 'demigirl', 'demitrans', 'female', 'femme', 'ftm', 'genderfluid', 'genderqueer', 'graygender', 'intergender', 'intersex', 'maverique', 'mtf', 'multigender', 'neutrois', 'nonbinary', 'novigender', 'omnigender', 'pangender', 'polygender', 'third gender', 'transfemme', 'transgender', 'transmasc', 'trigender', 'two-spirit'];

//variable for web cam capture
let capture;

//preload web cam capture
function preload() {
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);

//all things button related
button = createButton('store your gender here')
  button.size(360,30)
button.style('font-size', '20px')
  button.style('font-family', 'courier')
  button.position(width/2 - 150, height/2 + 110);
  button.mousePressed(words); //displays function made for arrays when button is pressed
}

function draw() {
  translate(windowWidth/2, windowHeight/2);
  //web cam
image(capture, -150, -125, 360,260);
}


//for loop displaying arrays, called in setup (all things button)
function words(){
  for (let x = 0; x < width; x += 80) {
    for (let y = 0; y < height; y += 20) {
      fill(random(255), random(255), random(255))
      rotate(random(PI));
      textFont('Courier');
      textSize(18);
      text(random(gender), x, y);
    }
      }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
