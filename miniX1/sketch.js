//declare variables
let lampOn, lampOff; 
let help = 'click the cat to turn on some light';
let congrats = 'congrats! you turned on the light';
let music;

//preloads images and sound
function preload() {
  lampOn = loadImage('cat_lamp_on.png');
  lampOff = loadImage('cat_lamp_off.png');
  music = loadSound('merry_go_round_of_life.mp3');
  music.setVolume(0.3);
}


function setup() {
  createCanvas(400, 400);
}


function draw() {
  image(lampOff, 0, 0); //displays image at actual size
  text(help,50,330); //placement of text
  textStyle(ITALIC);
  fill(250); // fill text color 
  textSize(20); //resize text
  stroke(250); // text outline  
  }
 
//decides what happens when clicked
function mousePressed() {
  music.stop(); // stops music from stacking
  lampOff = lampOn;
  help = congrats;
  music.play(); 
  

}
 
  
