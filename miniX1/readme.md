*View my project **[here](https://unblissful.gitlab.io/aesthetic-programming/miniX1/)***

*View my code **[here](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/blob/main/miniX1/sketch.js)***

![ ](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/raw/main/miniX1/cat_lamp_video.mp4)


I was browsing around for some inspiration as to what to do for my project and i came across this tarot card generator https://www.melaniehoff.com/generativetarot/ that I found to be quite cool and it didn't seem too intimidating to attempt something in the same fashion. And so a couple days prior to this exercise I had been practicing pixel art and made a lit up cat lamp and upon finding this tarot card generator and seeing various cards change image when clicked, I got inspired to make the lamp turn on when clicked.

And so I made a code where we first see a turned off cat lamp that then turns on when clicked and plays some music for ambience as well, here using one of my favourites, 'Merry-go-round of Life by Joe Hisaishi, from Howl's Moving Castle, (https://www.youtube.com/watch?v=f7SS57LFPco). 
The cat lamp turns on when clicked with the use of the `mousePressed()` function, where I switch from one loaded image to another by using the assignment operator `=` as well as using this for the text strings `''`.

I had a pretty clear idea of what i wanted to do and thereby wrote my code with the help of the p5 references https://p5js.org/reference/ and just searched around for what i thought might be useful as well as utilising some already existing knowledge i have e.g. declaring variables.

Coding this was quite rewarding as I could see what i had visualised come to life by writing the code myself. Because i already had a tiny bit of experience with coding the way of writing and the syntax wasn't completely foreign but coding definitely requires a little extra work from your side in relation to reading and making sure there are no errors compared to, say, writing and English essay.

I've never really considered what code and programming mean to me or rather I've never considered that they mean something to me. I've always been aware of coding and has for a long time thought it to be somewhat interesting and been something I've been meaning to learn to some extent. 
But finally being more hands on with it and having more freedom with it I find myself inspired to further explore what is possible and see how its meaning to me will evolve. 

I had originally thought of code and programming in a very technical and very office-based context but have come to the realization that it can be much more than that. In "Coding for Everyone and the Legacy of Mass Literacy" by Anette Vee she states that individual empowerment may perhaps be one of the current biggest motivators for coding literacy and moreover is 'a powerful technology for personal expression and information generation'
this definitely resonates with me and i highly suspect code and programming will be yet another way for me to express myself from here on out.


##### References:
---
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48

* Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93.

