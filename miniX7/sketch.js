// variables for text
let question = "DO YOU FEEL REPRESENTED?";
let flags = [];

// all the many flags, unfortunately not every existing one though </3
function preload() {
for(let i = 0; i < 25; i++) {

  // added the flags in an array this time
  flags[i] = loadImage('flags/flag'+i+'.png')
}
}


function setup() {
  createCanvas(windowWidth, windowHeight);
}


function draw() {
  background(110);

  pride();

   // the question
 represented();

  // smileys
  sadSmiley();
  midSmiley();
  happySmiley();
}

function represented(){
  textSize(40);
  textAlign(CENTER, CENTER);
  fill(255);
  strokeWeight(5);
  textStyle(BOLD);
  text(question, width/2, height/2 - 100);
}

// variables for smileys
let smileySize = 80;
let smileyX = 720;
let smileyY = 300;
let smileyPlace = 40;
let smileyEyesY = 60;
let eyesWidth = 10;
let eyesHeight = 20;


function sadSmiley() {
  //sad smiley head
  push();
  stroke(0);
  strokeWeight(1.5);
  fill(220, 34, 41);
  //added drop shadow
  drawingContext.shadowOffsetX = 5;
  drawingContext.shadowOffsetY = 5;
  drawingContext.shadowBlur = 10;
  drawingContext.shadowColor = 'black';
  ellipse(smileyX - 190, smileyY + smileyPlace, smileySize, smileySize);
  pop();

  //sad smiley eyes
  ellipseMode(CORNER);
  fill(0);
  ellipse(smileyX - 180, smileyY + smileyEyesY, eyesWidth, eyesHeight);
  ellipseMode(CORNER);
  fill(0);
  ellipse(smileyX - 150, smileyY + smileyEyesY, eyesWidth, eyesHeight);

  // sad smiley mouth
  noFill();
  strokeWeight(3);
  arc(smileyX - 175, smileyY + 90, -30, 30, 22, TWO_PI);
}


function midSmiley() {
  // mid smiley head
  push();
  stroke(0);
  strokeWeight(1.5);
  fill(253, 224, 26);
  drawingContext.shadowOffsetX = 0;
  drawingContext.shadowOffsetY = 5;
  drawingContext.shadowBlur = 10;
  drawingContext.shadowColor = 'black';
  ellipse(smileyX - 45, smileyY + smileyPlace, smileySize, smileySize);
  pop();

  //mid smiley eyes
  ellipseMode(CORNER);
  fill(0);
  ellipse(smileyX - 25, smileyY + smileyEyesY, eyesWidth, eyesHeight);
  ellipseMode(CORNER);
  fill(0);
  ellipse(smileyX + 5, smileyY + smileyEyesY, eyesWidth, eyesHeight);

  //mid smiley mouth
  noFill();
  strokeWeight(3);
  rect(smileyX - 20, smileyY + 95, 30, 1);
}


function happySmiley() {
  // happy smiley head
  push();
  stroke(0);
  strokeWeight(1.5);
  fill(0, 140, 64);
  drawingContext.shadowOffsetX = -5;
  drawingContext.shadowOffsetY = 5;
  drawingContext.shadowBlur = 10;
  drawingContext.shadowColor = 'black';
  ellipse(smileyX + 105, smileyY + smileyPlace, smileySize, smileySize);
represented()
  pop();


  //happy smiley eyes
  ellipseMode(CORNER);
  fill(0);
  ellipse(smileyX + 130, smileyY + smileyEyesY, eyesWidth, eyesHeight);
  ellipseMode(CORNER);
  fill(0);
  ellipse(smileyX + 160, smileyY + smileyEyesY, eyesWidth, eyesHeight);


  //happy smiley mouth
  noFill();
  strokeWeight(3);
  arc(smileyX + 133, smileyY + 80, -35, 25, 0, PI);
}

//variables for flags
let nextFlag = 0;
let nextColumn = 200;
let rowOneX = 17;
let nextRow = 295;
let rowTwoX = rowOneX + nextRow;
let rowThreeX = rowTwoX + nextRow;
let rowFourX = rowThreeX + nextRow;
let rowFiveX = rowFourX + nextRow;


//added a function for displaying flags + animation to make code more overseeable
function pride(){
  rowOne();
   rowTwo();
   rowThree();
   rowFour();
   rowFive();
nextFlag++;

//double the height to create a smoother flow
if(nextFlag > height*2){
  nextFlag = 0
}
  }

// flags
// changed to minus nextColumn to have flags start higher up thus contributing to a smoother looking animation.
function rowOne() {
  image(flags[18], rowOneX, nextFlag - nextColumn );
  image(flags[10], rowOneX, nextFlag - nextColumn*2);
  image(flags[22], rowOneX, nextFlag - nextColumn*3);
  image(flags[4], rowOneX, nextFlag - nextColumn*4);
}

function rowTwo() {
  image(flags[15], rowTwoX, nextFlag - nextColumn);
  image(flags[6], rowTwoX, nextFlag  - nextColumn* 2);
  image(flags[21], rowTwoX, nextFlag - nextColumn* 3);
  image(flags[14], rowTwoX, nextFlag - nextColumn*4);
}

function rowThree() {
  image(flags[8], rowThreeX, nextFlag - nextColumn);
  image(flags[16], rowThreeX, nextFlag - nextColumn*2);
  image(flags[0], rowThreeX, nextFlag - nextColumn* 3);
  image(flags[1], rowThreeX, nextFlag - nextColumn*4);
}

function rowFour() {
  image(flags[3], rowFourX, nextFlag -  nextColumn);
  image(flags[11], rowFourX, nextFlag - nextColumn*2);
  image(flags[7], rowFourX, nextFlag - nextColumn* 3);
  image(flags[13], rowFourX, nextFlag - nextColumn*4);
}

function rowFive() {
  image(flags[2], rowFiveX, nextFlag - nextColumn);
  image(flags[20], rowFiveX, nextFlag - nextColumn*2);
  image(flags[12], rowFiveX, nextFlag - nextColumn* 3);
  image(flags[9], rowFiveX, nextFlag - nextColumn*4);

}
