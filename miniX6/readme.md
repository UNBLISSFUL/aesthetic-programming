*View my project [here](https://unblissful.gitlab.io/aesthetic-programming/miniX6/)*

*View my sketch.js [here](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/blob/main/miniX6/sketch.js) and my class.js [here](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/blob/main/miniX6/class.js)*

![](minix6.gif)

___

I originally started my miniX6 with space invaders in mind and wanted to do a game somewhat like space invaders, which then proved to provide me with a bit of a challenge and thus I ended up with a simple laser shooting-like game where you have to score as many points as you can within 30 seconds.

I made my program with a lot of help from [this](https://www.youtube.com/watch?v=biN3v3ef-Y0) video by Daniel Shiffman. 
As well as [this](https://editor.p5js.org/denaplesk2/sketches/ryIBFP_lG) for making the countdown timer.

In this program I've made use of `class` both for the clouds as well as the little `rect` bullets. I've made the clouds work as a target of sorts, making them objects that are placed in a row and gives the player something to aim for. Furthermore I've made these rectangle bulelts red in order to simulate heat of some sort, originally with the thought of making the clouds evaporate in mind, thus reflecting wanting grey skies to disappear in wants of better weather, I however, could not manage to get my code working and evaporate the clouds and therefore ended up choosing to omit this action. Every time a bullet hits a cloud the score will increase by one and using `keyPressed` I've coded the rectangles to only appear when pressing the spacebar.

For the player I've chosen to gone with and umbrella, sticking with the weather theme. The umbrella is made using a `constructor function` and is moveable by pressing the left- and right arrow keys.

There's of course a certain abstraction involved when it comes to object oriented programming. When you realise something as an object in computing there are countless more thigns you have to take into account.
Suddenly you have to think of every action you want your object to make and you open up for making all these, however big or small, actions possible opposed to if the object was the real life thing or being.

> There is a history to each of these objects and their development, which means that the parameters for interaction are determined by a series of more or less successful abstractions of a peculiarly composite, multilayered and stratified kind.

Matthew Fuller and Andrew Goffey writes such in the chapter “The Obscure Objects of Object Orientation,” in How to be a Geek: Essays on the Culture of Software (Cambridge: Polity, 2017), which goes to show that the objects interactions and actions are determined by this creative abstraction.

As mentioned in "Object abstraction", Aesthetic Programming: A Handbook of Software Studies by Soon and Cox (2020), abstraction takes places in a way which makes us able to select certain behaviours and properties that we find relevant but still stimulating real and/or imaginary things and beings. 

---
##### References:

* Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
* Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017)
