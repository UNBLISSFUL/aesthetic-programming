let clouds = [];
let player;
let score = 0;
let heatwaves = [];
let timer = 30;
let timerStarted = 0;
let rules = 'Shoot as many clouds as you can within 30 seconds!';
let instruct1 = 'Use arrow keys to move umbrella left and right';
let instruct2 = 'Press space to shoot from the umbrella';
let instruct3 = 'Press any key to begin'
let tallyX = 115;
let tallyY = 240;

function preload(){
  cloud = loadImage('assets/cloud.png');
  umbrella = loadImage('assets/umbrella.png');
}


function setup() {
  createCanvas(windowWidth, windowHeight);
//for loop with clouds array and class
    for (let i = 0; i < 10; i++) {
      let x = width/6 + 100 * i;
      clouds[i] = new Cloud(x,30,30);
    }
//constructor function umbrella/player
    player = new Umbrella();
    textAlign(CENTER);
}


function draw() {
  background(100);
  gameStart();

  textSize(40);
  fill(0);
  text('SCORE: ' + score, tallyX, tallyY);

  showClouds();
  player.show();
  player.move();
  showHeatwaves();
  gameOver();
}


function showHeatwaves(){
  for (let i = 0; i < heatwaves.length; i++) {
    heatwaves[i].show();
  heatwaves[i].move();
//score counts 1, when heatwave hits cloud
  for (let j = 0; j < clouds.length; j++) {
   if (heatwaves[i].hits(clouds[j])){
        score = i+1;
      }
    }
  }
}


function gameStart(){
  fill(0);
  textSize(40);
    text(rules, width/2, height/2 - 100);

  fill(0);
  textSize(35);
    text(instruct1, width/2, height/2 + 5);
    text(instruct2, width/2, height/2 + 40);
    text(instruct3, width/2, height/2 + 110);
}


function showClouds() {
    for (let i = 0; i < clouds.length; i++) {
  clouds[i].show();
  }
}

//prevents umbrella from just sliding off canvas
function keyReleased(){
  player.setDir(0);
}


function keyPressed() {
//timer starts and counts every second when a key is pressed
  if (timerStarted == 0) {
    setInterval(timerInt, 1000);
    timerStarted = 1;
    clearWords();
  }

// heatwaves only shown if spacebar is pressed
  if (key ===' ') {
    var heatwave = new Heatwave(player.x + 40, height - 80);
    heatwaves.push(heatwave);
  }

//umbrella moving with use of arrow keys
  if (keyCode === RIGHT_ARROW) {
    player.setDir(1);
 } else if(keyCode === LEFT_ARROW) {
   player.setDir(-1);
 }
 return false;
}

// removes words when any key is pressed
function clearWords(){
  text(rules = ' ', 0, 0);
  text(instruct1 = ' ', 0, 0);
  text(instruct2 = ' ', 0, 0);
  text(instruct3 = ' ', 0, 0);
}

function timerInt() {
  if (timer > 0) {
    timer--;
  }
}


function gameOver() {
  if (timer >= 10) {
    textSize(40);
    text('TIME: 0:' + timer, tallyX, tallyY + 60);
  }
  if (timer < 10) {
    textSize(40);
    text('TIME: 0:0' + timer, tallyX, tallyY + 60);
  }
  if (timer == 0) {
    textSize(100);
    text('GAME OVER', width/2, height/2);
    noLoop();
  }
}


function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
