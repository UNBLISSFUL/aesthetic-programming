class Cloud {
  constructor(tempX, tempY){
    this.x = tempX;
    this.y = tempY;
  }

    show(){
      image(cloud, this.x, this.y);
      cloud.resize(90, 90);
    }
}


//constructor function used to create object
function Umbrella(){
  this.x = width/2;
  this.xdir = 0;

  this.show = function() {
    image(umbrella, this.x, height-100);
    umbrella.resize(90,90);
  }

this.setDir = function(dir){
  this.xdir = dir;
}

// how many pixels the umbrella moves
  this.move = function(dir) {
    this.x += this.xdir*5;
  }
}


class Heatwave {
  constructor(tempX, tempY){
    this.x = tempX;
    this.y = tempY;
    this.w = 5;
    this.h = 10;
  }

    show(){
      fill(255, 0, 0);
      noStroke();
      rect(this.x, this.y, this.w*2, this.h*2);
    }

    hits(clouds){
      let d = dist(this.x, this.y, clouds.x, clouds.y);
      if (d < 50){
        return true;
      } else{
        return false;
    }
  }
  move(){
    this.y = this.y -10;
  }
}
