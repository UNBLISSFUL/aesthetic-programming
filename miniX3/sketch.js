function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(4);
}

function draw() {
  background(245, 233, 170, 80);
  throbber();
}

function throbber() {
  
//blue notepad lines made with for-loop
    for (let i = 0; i < height; i += 20) {
      stroke(0, 128, 255, 20);
      strokeWeight(2);
    line(0, i, width, i);
    }
  
//red margin line
  stroke(255, 0, 0, 30);
  line(100, 0, 100, windowHeight);

  translate(width/2, height/2);
  
//digital clock
  push();
    let h = hour();
    let m = minute();
    let s = second();
      textAlign(CENTER);
      textSize(20);
      noStroke();
      textFont('Georgia');
      text(h+ ':' +m+ ':' +s, 0, 0);
  pop();
  
  push();
    let num = 12;
//movement of text in the round
    let cir = 360/12*frameCount; 
      rotate(radians(cir));
      textSize(40);
      fill(10, 130, 50);
      textFont('Georgia');
      text('wait', 50, 55);
  pop();
   
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}