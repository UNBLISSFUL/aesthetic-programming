*View my project [here](https://unblissful.gitlab.io/aesthetic-programming/miniX3/)*

*View my code [here](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/blob/main/miniX3/sketch.js)*

![](throbber.gif)



> *[...] how the actual experience of time is shaped is strongly influenced by all sort of design decisions and implementations, [...]*

Hans Lammerant writes so in "How humans and machines negotiate experience of time" in *The Techno-Galactic Guide to Software Observation (2018).* While making my miniX3 I had this quote in mind and it resulted in me making changes as I went along trying to somehow express time.

I made this code by mainly tinkering with the source code for figure 3.3 in Soon & Cox "Infinite Loops" in Aesthetic Programming: A Handbook of Software Studies an thus ended up making quite a simple throbber with a look similar to what we would usually associate with a loading screen and chose the word 'wait' to continuously appear since throbbers are used when there's some form of waiting time. 
At the same time the use of the word wait makes one feel like they are wating *for* something, except here you would just endlessly wait.


Lammerant also writes:
>*This re-enactment of such cycles [sundials] through the computer will remind human users that the experience of time is a socially negotiated and technically implemented experience.*

This compelled me to add the digital clock (following a sundial of course), the clock being yet another way of depicting time, I've made use of time related syntaxes here; `hour()`, `minute()` and `second()`, which are undoubtedly time related in that these display the current hour, minute and second. 
I originally had a higher frameRate, but ended up lowering it to create a sense of impatience, when the circle of 'waits' and the digital clock are in play together this way time feels slower when looking at the word wait but with the digital clock you see the current time and know that time is passing at the same speed, it just appears slower with a lower frameRate and thus creating a sense of impatience because, again, you would associate the project with a loading screen, *waiting* for something to happen or appear. 

 Furthermore I chose to go with this look by making blue lines, with the use of a `for() loop` to create an association to notepads, something not inherently technological and is thus a little note to non-technological means of knowing, seeing and understanding time. This is meant in the sense that most people, at least among the ones I know, use and prefer to use a digital clock to check their time, typically through their phone or laptop and therefore perhaps disconnecting time even more from something physical than previously. Not that time is necessarily conencted to something physical, but if you go even just a few years back you'd associate checking the time with looking at a clock on the wall or looking at your wristwatch, seeing the clock hands move second by second, minute by minuteand hour by hour almost making time tangible in a completely different way from current times, even though time has never been something physical.

 This whole thing is meant as a reflection or at least something to induce a reflection on what time is and how time conceptually, perhaps emotioanlly, could differ from person to person, media to media etc. But nonetheless I find that even across different media or anywhere one would come across anything like it, most would associated anything moving in a continuous pattern like buffering or loading and whatnot would associate this with a throbber.


##### References:
___
* https://p5js.org/reference/
* Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
* Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190.
