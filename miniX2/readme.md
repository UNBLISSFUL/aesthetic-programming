view my project **_[here](https://unblissful.gitlab.io/aesthetic-programming/miniX2/)_**

view my code **_[here](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/blob/main/miniX2/sketch.js)_**

![ ](https://gitlab.com/UNBLISSFUL/aesthetic-programming/-/raw/main/miniX2/preview.png)


For my miniX2 I made a little program that displays different LGBTQ+ flags in the background e.g, the progress pride flag, the panromantic flag, the genderqueer flag etc. and ask the question *'DO YOU FEEL REPRESENTED?'* along with 3 smileys in red, yellow and green which show three different moods and are meant to represent feelings on whether one feels represented, here in relation to queerness. I made the flags move down the height of the window soo as to make the project more dynamic, moreover i changed the faces of the red and green smiley to point in opposites direction, since making them face straightforward, with likeliness to the yellow one, looked to static with the moving flags.

With this miniX I had an extremely hard time figuring out what i wanted to make and frankly the project itself unfolded as i was coding, without having a clear image of what I wanted to portray. I knew that I wanted to do something that was meaningful to me and after reading "Modifying the Universal" by Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, being a POC myself, I was considering whether I should do something related to representation of POC and different ethnicities however I wasn't sure what to do or how to express my thoughts on this. 

> *Does the mechanism of skintone modifiers really bring diversity to the emoji project?*

Abbing, Pierrot and Snelting asks this question in "Modifying the Universal" and I also contemplated this while coming up with ideas. I personally have most my emojis on the default yellow tone and rarely use any of the humanoid emojis, therefore i considered making an emoji of sorts tht completely strayed away from being human-like however I would seemingly circle back to a plain yellow circle emoji, which I then felt wouldn't convey what i wanted to address. This is of course subjective but in my opinion the inclusion of humanoid emojis and the extra addition of such a large number of them along with skintone modifiers, although trying to be inclusive, makes it harder to feel represented because then some will be represented where others won't, whereas if it was non human-like emojis we might not even reflect ourselves in these and therefore not ponder on whether we feel seen and represented, consequently you end up being exclusive by trying to be inclusive.

Being queer I also wanted to do something related to representation of the LGBTQ+ community and so I went on my phone and scrolled through the emojis to see what representation we currently have and although there are queer families in the humanoid emojis I stumbled upon something when I had scrolled to the end and reached the flags. There are so many different flags for different sexualities and genders but when I was at the flag section of the emojis the only two LGBTQ+ flags were the rainbow pride flag ass well as the transgender flag. I found this curious and therefore decided I wanted to include different flags and hence ending up with my project.

I do realize that I also have not been able to include every single queer flag and we therefore circle back to being exclusive by trying to be inclusive as well as queerness not being as accepted everywhere and once again we're circling back to being exclusive. 
However these flags are meaningful to not only me but to many other queer people as well, as a means of selfexpression and identity.


##### References:
---
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70

* Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51





