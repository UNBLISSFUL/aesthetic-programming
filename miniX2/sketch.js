// variables for text
let question = "DO YOU FEEL REPRESENTED?";
let questionX = 720;
let questionY = 300;

// all the many flags, unfortunately not every existing one though </3
function preload() {
  abro = loadImage("abrosexual.png");
  age = loadImage("agender.png");
  allo = loadImage("allosexual.png");
  andro = loadImage("androgyne.png");
  aro = loadImage("aromantic.png");
  ace = loadImage("asexual.png");
  bi = loadImage("bisexual.png");
  demiB = loadImage("demiboy.png");
  demiG = loadImage("demigirl.png");
  demiR = loadImage("demiromantic.png");
  demiS = loadImage("demisexual.png");
  mlm = loadImage("gay.png");
  genFluid = loadImage("genderfluid.png");
  genQueer = loadImage("genderqueer.png");
  inter = loadImage("intersex.png");
  wlw = loadImage("lesbian.png");
  enby = loadImage("nonbinary.png");
  omni = loadImage("omnisexual.png");
  panR = loadImage("panromantic.png");
  panS = loadImage("pansexual.png");
  polyA = loadImage("polyamory.png");
  polyS = loadImage("polysexual.png");
  queer = loadImage("rainbow.png");
  progress = loadImage("progress.png");
  trans = loadImage("transgender.png");
}


function setup() {
  createCanvas(windowWidth, windowHeight);
}


function draw() {
  background(220);

  // flags
  rowOne();
  rowTwo();
  rowThree();
  rowFour();
  rowFive();

  // the question
  textSize(35);
  textAlign(CENTER, CENTER);
  noFill();
  strokeWeight(2);
  textStyle(BOLD);
  text(question, questionX, questionY);

  // smileys
  sadSmiley();
  midSmiley();
  happySmiley();
}


// variables for smileys
let smileySize = 80;

//used in relation to the text
let smileyPlace = 40;
let smileyEyesY = 60;
let eyesWidth = 10;
let eyesHeight = 20;

function sadSmiley() {
  //sad smiley head
  stroke(0);
  strokeWeight(1.5);
  fill(220, 34, 41);
  ellipse(questionX - 190, questionY + smileyPlace, smileySize, smileySize);

  //sad smiley eyes
  ellipseMode(CORNER);
  fill(0);
  ellipse(questionX - 180, questionY + smileyEyesY, eyesWidth, eyesHeight);
  ellipseMode(CORNER);
  fill(0);
  ellipse(questionX - 150, questionY + smileyEyesY, eyesWidth, eyesHeight);

  // sad smiley mouth
  noFill();
  strokeWeight(3);
  arc(questionX - 175, questionY + 90, -30, 30, 22, TWO_PI);
}


function midSmiley() {
  // mid smiley head
  stroke(0);
  strokeWeight(1.5);
  fill(253, 224, 26);
  ellipse(questionX - 45, questionY + smileyPlace, smileySize, smileySize);

  //mid smiley eyes
  ellipseMode(CORNER);
  fill(0);
  ellipse(questionX - 25, questionY + smileyEyesY, eyesWidth, eyesHeight);
  ellipseMode(CORNER);
  fill(0);
  ellipse(questionX + 5, questionY + smileyEyesY, eyesWidth, eyesHeight);

  //mid smiley mouth
  noFill();
  strokeWeight(3);
  rect(questionX - 20, questionY + 95, 30, 1);
}


function happySmiley() {
  // happy smiley head
  stroke(0);
  strokeWeight(1.5);
  fill(0, 140, 64);
  ellipse(questionX + 105, questionY + smileyPlace, smileySize, smileySize);

  //happy smiley eyes
  ellipseMode(CORNER);
  fill(0);
  ellipse(questionX + 130, questionY + smileyEyesY, eyesWidth, eyesHeight);
  ellipseMode(CORNER);
  fill(0);
  ellipse(questionX + 160, questionY + smileyEyesY, eyesWidth, eyesHeight);

  //happy smiley mouth
  noFill();
  strokeWeight(3);
  arc(questionX + 133, questionY + 80, -35, 25, 0, PI);
}

//variables for flags
let start = 0;
let nextFlag = 200;
let rowOneX = 17;
let nextRow = 295;
let rowTwoX = rowOneX + nextRow;
let rowThreeX = rowTwoX + nextRow;
let rowFourX = rowThreeX + nextRow;
let rowFiveX = rowFourX + nextRow;

// flags
function rowOne() {
  image(panR, rowOneX, start);
  start++;
  // when first flag hits bottom --> starts again at the top
  if (start > windowHeight) start = 0; 
  image(demiS, rowOneX, start - nextFlag);
  image(progress, rowOneX, start - nextFlag * 2);
  image(aro, rowOneX, start - nextFlag * 3);
}

function rowTwo() {
  image(wlw, rowTwoX, start);
  image(bi, rowTwoX, start - nextFlag);
  image(polyS, rowTwoX, start - nextFlag * 2);
  image(inter, rowTwoX, start - nextFlag * 3);
}

function rowThree() {
  image(demiG, rowThreeX, start);
  image(enby, rowThreeX, start - nextFlag);
  image(abro, rowThreeX, start - nextFlag * 2);
  image(age, rowThreeX, start - nextFlag * 3);
}

function rowFour() {
  image(andro, rowFourX, start);
  image(mlm, rowFourX, start - nextFlag);
  image(demiB, rowFourX, start - nextFlag * 2);
  image(genQueer, rowFourX, start - nextFlag * 3);
}

function rowFive() {
  image(allo, rowFiveX, start);
  image(polyA, rowFiveX, start - nextFlag);
  image(genFluid, rowFiveX, start - nextFlag * 2);
  image(demiR, rowFiveX, start - nextFlag * 3);
}
